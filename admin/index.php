<?php
/*
  $Id: index.php,v 1.2 2004/03/09 19:56:29 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/

  require('../includes/configure.php');
header("Location : ".HTTPS_SERVER.DIR_WS_HTTPS_CATALOG.'admin/');
?>