<?php
/*
  $Id: pages.php,v 1.2 2004/03/05 00:36:42 ccwjr Exp $

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
  
  Chain Reaction Works, Inc
  
  Copyright &copy; 2006 Chain Reaction Works, Inc
  
  Last Modifed By : $Author$
  Last Modified On :  $Date$
  Latest Revision :  $Revision$
  
  
*/

define('NAVBAR_TITLE', 'Pages');

define('HEADING_TITLE', 'Pages');
define('TEXT_NO_PAGES', 'There are no pages to list in this category.');
define('TEXT_NO_CATEGORIES', 'There are no categories to list yet.');
?>
