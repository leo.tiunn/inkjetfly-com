<?php
/*
$Id: usps.php 1739 2011-09-01 00:52:16Z hpdl $

osCommerce, Open Source E-Commerce Solutions
http://www.oscommerce.com

Copyright � 2002 osCommerce

Released under the GNU General Public License
*/

define('MODULE_SHIPPING_USPS_TEXT_TITLE', 'United States Postal Service&nbsp;');
define('MODULE_SHIPPING_USPS_TEXT_DESCRIPTION', 'United States Postal Service<br><br>You will need to have registered an account with USPS at https://secure.shippingapis.com/registration/ to use this module<br><br>USPS expects you to use pounds as weight measure for your products.');
define('MODULE_SHIPPING_USPS_TEXT_ERROR', 'An error occured with the USPS shipping calculations.<br>If you would like to use USPS as your shipping method, please contact the store owner.');
define('MODULE_SHIPPING_USPS_TEXT_DAY', 'day');
define('MODULE_SHIPPING_USPS_TEXT_DAYS', 'days');
define('MODULE_SHIPPING_USPS_TEXT_WEEKS', 'weeks');
define('MODULE_SHIPPING_USPS_TEXT_BUSINESS', 'business');
define('MODULE_SHIPPING_USPS_TEXT_VARIES', 'varies by country');
define('MODULE_SHIPPING_USPS_TEXT_ESTIMATED', 'Approximate delivery time from mailing date ');
define('MODULE_SHIPPING_USPS_TEXT_INSURED', 'Insured for ');
define('MODULE_SHIPPING_USPS_TEXT_WEIGHT_DISPLAY', ' (%d pkg, %01.2f lbs total)');
define('MODULE_SHIPPING_USPS_TEXT_CONNECTION_ERROR', 'Failed to connect to the USPS postage rates server.');
?>